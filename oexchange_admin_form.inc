<?php
/**
 * @file
 * This has contain supporting function for admin of oexchange module.
 */

/**
 * Implements hook_form().
 */
function oexchange_admin_form($form, &$form_state) {
  $form = array();
  $form['oexchange_property_vendor'] = array(
    '#title' => t('Vendor'),
    '#description' => t('Human readable name of the target vendor.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('oexchange_property_vendor'),
  );
  $form['oexchange_property_title'] = array(
    '#title' => t('Title'),
    '#description' => t('Human readable long title for the target.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('oexchange_property_title'),
  );
  $form['oexchange_property_name'] = array(
    '#title' => t('Name'),
    '#description' => t('Human readable short name of the target.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('oexchange_property_name'),
  );
  $form['oexchange_property_prompt'] = array(
    '#title' => t('User prompt'),
    '#description' => t('Human readable call to action for sending links to this target.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('oexchange_property_prompt'),
  );
  return system_settings_form($form);
}
