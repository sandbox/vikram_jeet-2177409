<?php

/**
 * @file
 * This has contain supporting function of oexchange module.
 */

/**
 * Implements hook_form().
 */
function oexchange_share_form($form, &$form_state) {
  if (!user_access('share content')) {
    drupal_goto('user/login', array('query' => drupal_get_destination()));
  }
  $oexchange_data = $_GET;
  $oexchange_url = '';
  $oexchange_title = '';
  $oexchange_tags = '';
  $oexchange_description = '';
  $oexcahnge_ctype = '';
  $oexcahnge_imageurl = '';
  $oexchange_swfurl = '';
  $oexchange_screenshot = '';
  $oexchange_iframeurl = '';
  $oexchange_height = '';
  $oexchange_width = '';
  $oexchangeimage_src = array();
  $basepath = drupal_get_path('module', 'oexchange');
  if (array_key_exists('url', $oexchange_data)) {
    $oexchange_url = urldecode($oexchange_data['url']);
    if (filter_var($oexchange_url, FILTER_VALIDATE_URL)) {
      $output = drupal_http_request($oexchange_url);
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    drupal_access_denied();
  }
  // Check title is exists in url or not.
  $title_exists = array_key_exists('title', $oexchange_data);
  if ($title_exists && !empty($oexchange_data['title'])) {
    $oexchange_title = $oexchange_data['title'];
  }
  else {
    $title = array();
    if (strlen($output->data) > 0) {
      preg_match("/\<title\>(.*)\<\/title\>/", $output->data, $title);
      $oexchange_title = $title[1];
    }
  }
  // Check description is exists in url or not.
  $description_exists = array_key_exists('description', $oexchange_data);
  if ($description_exists && !empty($oexchange_data['description'])) {
    $oexchange_description = $oexchange_data['description'];
  }
  else {
    preg_match("/<meta name=\"description\" content=\"([^<]*)\"([^<]*)>/",
                $output->data, $meta_tags);
    $oexchange_description = $meta_tags[1];
  }
  // Check ctype is exists in url or not.
  $ctype_exists = array_key_exists('ctype', $oexchange_data);
  if ($ctype_exists && !empty($oexchange_data['ctype'])) {
    $oexcahnge_ctype = $oexchange_data['ctype'];
    // Check ctype value is image.
    $ctype_image = $oexchange_data['ctype'] == 'image';
    // Check imageurl is exists in url or not.
    $imageurl_exists = array_key_exists('imageurl', $oexchange_data);
    // Check is imageurl empty in url.
    $imageurl_empty = !empty($oexchange_data['imageurl']);
    if ($ctype_image && $imageurl_exists && $imageurl_empty) {
      $oexcahnge_imageurl = $oexchange_data['imageurl'];
    }
    // Check ctype value is flash.
    $ctype_flash = $oexchange_data['ctype'] == 'flash';
    // Check swfurl is exists in url or not.
    $swfurl_exists = array_key_exists('swfurl', $oexchange_data);
    // Check is swfurl empty in url.
    $swfurl_empty = !empty($oexchange_data['swfurl']);
    if ($ctype_flash && $swfurl_exists && $swfurl_empty) {
      $oexchange_swfurl = $oexchange_data['swfurl'];
      // Check screenshot is exists in url or not.
      $swfurl_exists = array_key_exists('screenshot', $oexchange_data);
      if ($swfurl_exists && !empty($oexchange_data['screenshot'])) {
        $oexchange_screenshot = $oexchange_data['screenshot'];
      }
    }
    // Check ctype value is iframe.
    $ctype_iframe = $oexchange_data['ctype'] == 'iframe';
    // Check iframeurl is exists in url or not.
    $iframeurl_exists = array_key_exists('iframeurl', $oexchange_data);
    // Check is iframeurl empty in url.
    $iframeurl_empty = !empty($oexchange_data['iframeurl']);
    if ($ctype_iframe && $iframeurl_exists && $iframeurl_empty) {
      $oexchange_iframeurl = $oexchange_data['iframeurl'];
    }
    // Check height is exists in url or not.
    $height_exists = array_key_exists('height', $oexchange_data);
    if ($height_exists && !empty($oexchange_data['height'])) {
      $oexchange_height = $oexchange_data['height'];
    }
    // Check width is exists in url or not.
    $width_exists = array_key_exists('width', $oexchange_data);
    if ($width_exists && !empty($oexchange_data['width'])) {
      $oexchange_width = $oexchange_data['width'];
    }
  }
  else {
    $image = array();
    if (strlen($output->data) > 0) {
      $image_src = '/<img[^>]+src\s*=\s*["\']?([^"\' ]+)[^>]*>/';
      preg_match_all($image_src, $output->data, $image);
      if (count($image[1]) > 0) {
        $oexcahnge_imageurl = $image[1][0];
        $oexchangeimage_src = $image[1];
      }
    }
  }
  drupal_add_js('var oexchange_image;
        oexchange_image =' . json_encode($oexchangeimage_src) . ';', 'inline');
  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => $basepath . '/css/oexchange_style.css',
    ),
    'js' => array(
      'type' => 'file',
      'data' => $basepath . '/js/image_shift.js',
    ),
  );
  // Check tags is exists in url or not.
  $tags_exists = array_key_exists('tags', $oexchange_data);
  if ($tags_exists && !empty($oexchange_data['tags'])) {
    $oexchange_tags = $oexchange_data['tags'];
  }
  $form['oexchange_field_write_own'] = array(
    '#title' => t('write in your mind'),
    '#type' => 'textarea',
  );
  $form['oexchange_images'] = array(
    '#type' => 'item',
    '#markup' => '<img class = "oexchange_main_image" index =0 width = "250" height="250" src= "' . $oexcahnge_imageurl . '"/>',
  );
  $form['oexchange_previous_button'] = array(
    '#type' => 'item',
    '#markup' => '<img class = "oexchange_image_previous" src ="' . $basepath . '/images/prev-botton2.png" />',
  );
  $form['oexchange_next_button'] = array(
    '#type' => 'item',
    '#markup' => '<img class = "oexchange_image_next" src ="' . $basepath . '/images/next-botton2.png" />',
  );
  $form['oexchange_field_title'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_title,
  );
  $form['oexchange_title'] = array(
    '#type' => 'item',
    '#markup' => '<h1>' . $oexchange_title . '</h1>',
  );
  $form['oexchange_description'] = array(
    '#type' => 'item',
    '#markup' => $oexchange_description,
  );
  $form['oexchange_field_site_url'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_url,
  );
  $form['oexchange_field_description'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_description,
  );
  $form['oexchange_field_tags'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_tags,
  );
  $form['oexchange_field_ctype'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexcahnge_ctype,
  );
  $form['oexchange_field_swfurl'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_swfurl,
  );
  $form['oexchange_field_imageurl'] = array(
    '#title' => '',
    '#type' => 'textfield',
    '#default_value' => $oexcahnge_imageurl,
  );
  $form['oexchange_field_iframeurl'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_iframeurl,
  );
  $form['oexchange_field_height'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_height,
  );
  $form['oexchange_field_width'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_width,
  );
  $form['oexchange_field_screenshot'] = array(
    '#type' => 'hidden',
    '#default_value' => $oexchange_screenshot,
  );
  $form['share_button'] = array(
    '#title' => t('send'),
    '#type' => 'submit',
    '#value' => t('Share'),
  );
  $form['#theme'] = 'oexchange_content';
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function oexchange_share_form_submit($form, &$form_state) {
  global $language_content;
  global $user;
  $lang = $language_content->language;
  $node = new stdClass();
  $node->type = 'oexchange';
  $node->language = $lang;
  $node->uid = $user->uid;
  $node->title = $form_state['values']['oexchange_field_title'];
  $node->oexchange_field_write_own[$lang][0]['value']
    = $form_state['values']['oexchange_field_write_own'];
  $node->oexchange_field_url[$lang][0]['value']
    = $form_state['values']['oexchange_field_site_url'];
  $node->oexchange_field_description[$lang][0]['value']
    = $form_state['values']['oexchange_field_description'];
  $node->oexchange_field_ctype[$lang][0]['value']
    = $form_state['values']['oexchange_field_ctype'];
  $node->oexchange_field_tags[$lang][0]['value']
    = $form_state['values']['oexchange_field_tags'];
  $node->oexchange_field_image[$lang][0]['value']
    = $form_state['values']['oexchange_field_imageurl'];
  $node->oexchange_field_swfurl[$lang][0]['value']
    = $form_state['values']['oexchange_field_swfurl'];
  $node->oexchange_field_iframeurl[$lang][0]['value']
    = $form_state['values']['oexchange_field_iframeurl'];
  $node->oexchange_field_height[$lang][0]['value']
    = $form_state['values']['oexchange_field_height'];
  $node->oexchange_field_width[$lang][0]['value']
    = $form_state['values']['oexchange_field_width'];
  $node->oexchange_field_screenshot[$lang][0]['value']
    = $form_state['values']['oexchange_field_screenshot'];
  $oexchange_node = node_submit($node);
  node_save($oexchange_node);
  drupal_set_message(t('your sharing details has been saved.'), 'status');
}

/**
 * Function oexchange_xrd is used to generate xrd.
 */
function oexchange_xrd() {
  $vendor = variable_get('oexchange_property_vendor');
  $title = variable_get('oexchange_property_title');
  $name = variable_get('oexchange_property_name');
  $prompt = variable_get('oexchange_property_prompt');
  $end_point = url('share', array('absolute' => TRUE));
  $img16 = variable_get('oexchange_image16');
  $img32 = variable_get('oexchange_image32');
  $string = <<<EOD
<?xml version='1.0' encoding='UTF-8'?>
  <!--
      This XRD document describes the capabilities of a single OExchange Target.
      It may be obtained directly or via a reference from a host's .well-known/host-meta.

      The "resource" that this XRD describes is the OExchange Target, which is an
      implicit service being provided at a particular host.   
  -->
  <XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">

      <!-- 
          Every OExchange Target is uniquely identified by a URI.  
          Typically, this is the primary URI of the service, which a user could
          reasonably browse to.
          This URI must be present as the Subject of this XRD document.
      -->
      <Subject>$end_point</Subject>

      <!-- 
          A set of Property elements define OExchange-specific attributes of this Target.
      -->
      <Property type="http://www.oexchange.org/spec/0.8/prop/vendor">$vendor</Property>
      <Property type="http://www.oexchange.org/spec/0.8/prop/title">$title</Property>
      <Property type="http://www.oexchange.org/spec/0.8/prop/name">$name</Property>
      <Property type="http://www.oexchange.org/spec/0.8/prop/prompt">$prompt</Property>

      <!-- 
          The Target's icon is expressed as a Link of a defined relation.
      -->
      <Link 
          rel= "icon" 
          href="$img16"
          type="image/png" 
          />
      <Link 
          rel= "icon32" 
          href="$img32"
          type="image/png" 
          />

      <!-- 
          The individual OExchange protocol endpoints are expressed as Links of a
          defined relation type.
      -->
      <Link 
          rel= "http://www.oexchange.org/spec/0.8/rel/offer" 
          href="$end_point"
          type="text/html" 
          />
  </XRD>
EOD;
  echo $string;
  drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');
}
