-------------------------------------------------------------------------------
                            OExchange
-------------------------------------------------------------------------------

OExchange is an open protocol for sharing any URL with any service on the web.

Intallation
-----------

Copy oexchange to your module directory and then enable on the admin modules 
page.

Configuration
-------------
admin/config/content/share_button
Change your configuration
Test you XRD on 
http://www.oexchange.org/tools/discoveryharness/
Put you XRD path - $base_url/oexchange.xrd

Test your application on
http://www.oexchange.org/tools/sourceharness/
Your Endpoint is - $base_url/share

For other site developers
--------------------------
Add <example.com> icon on your website / blog etc. and share links to
 example.com.

Follow below steps to add the Icon on your website -

Add below lines into the head

<script type="text/javascript"
src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script id="oexchange_script_id" type="text/javascript" src=
"http://example.com/sites/all/modules/custom/oexchange/js/oexchange_share.js">
</script>

Add below lines into the body, where you want to show the SportTab Share icon

<div class="drupal_share_button" share:title="title" share:url=
"http://yahoo.com">
  <a href="" class="share_button ico_32x32"></a>
</div>
 
Optional Fields in above –
share:title
share:url

Icon Sizes -
For large icon use ico_32x32 class in a tag
For small icon use ico_16x16 class in a tag

Author
------
Vikram Jeet
vikram@incaendo.com

Maintainer
----------
Nitin Kumar Singh
nitin@incaendo.com
