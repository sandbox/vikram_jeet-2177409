<?php
/**
 * @file
 * This file show the form of share.
 * @var $form This variable contain the form array.
 */
?>
<div class="oexchange_form">
  <?php
    print drupal_render($form['oexchange_field_write_own']);
  ?>
  <div class="oexchange_form_bottom_div">
    <div class="oexchange_form_image_buttons">
      <?php
        print drupal_render($form['oexchange_images']);
        print drupal_render($form['oexchange_previous_button']);
        print drupal_render($form['oexchange_next_button']);
      ?>
    </div>
    <div class="oexchange_form_title_description">
      <?php
        print drupal_render_children($form);
      ?>
    </div>
  </div>
</div>
