/**
 * @file
 * Image gallery for the OExchange module.
 */

jQuery(document).ready(function () {
  imageCount = oexchange_image.length;
  if(imageCount < 1) {
    jQuery('.oexchange_image_next').hide();
    jQuery('.oexchange_image_previous').hide();
  }
  jQuery('#edit-oexchange-field-imageurl').hide();
  jQuery('.oexchange_image_next').click(function() {
    image_index = jQuery('.oexchange_main_image').attr('index');
    image_index++;
    if(image_index > imageCount - 1) {
      image_index = 0;
    }
    jQuery('.oexchange_main_image').attr('index', image_index);
    jQuery('.oexchange_main_image').attr('src', oexchange_image[image_index]);
    jQuery('#edit-oexchange-field-imageurl').val(oexchange_image[image_index]);
  });
  jQuery('.oexchange_image_previous').click(function() {
    image_index = jQuery('.oexchange_main_image').attr('index');
    image_index--;
    if(image_index < 0) {
      image_index = imageCount - 1;
    }
    jQuery('.oexchange_main_image').attr('index', image_index);
    jQuery('.oexchange_main_image').attr('src', oexchange_image[image_index]);
    jQuery('#edit-oexchange-field-imageurl').val(oexchange_image[image_index]);
  });
});
