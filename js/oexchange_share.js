/**
 * @file
 * Used to share the content with OExchange module integrated site.
 */

jQuery(document).ready(function() {
  $drupalShareBtn = jQuery('.drupal_share_button');
  oexchange_src = jQuery('#oexchange_script_id').attr('src');
  imageUrl = oexchange_src.substring(0,oexchange_src.lastIndexOf('/', oexchange_src.lastIndexOf('/') - 1));
  $drupalShareBtn.each(function() {
    var $this = jQuery(this);
    var className = $this.children('a').attr('class');
    sizeImage = className.substring(className.lastIndexOf('_') + 1);
    image_share = '';
    if(sizeImage == '32x32') {
      image_share = 'shareimage_32.png';
      $this.css('width', '35px');
    }
    if(sizeImage == '16x16') {
      image_share = 'shareimage_16.png';
      $this.css('width', '19px');
    }
    html = '<img class = "image_id" src = "' + imageUrl + '/images/' + image_share + '"/>';
    $this.html(html);
  });
 $drupalShareBtn.click(function() {
   var $this = jQuery(this);
   var url = '';
   var title = '';
   var description = '';
   var imageurl = '';
   var oexchange_url;
   var viewUrl;
   oexchange_url = oexchange_src.split('/');
   oexchangeUrl = $this.attr('share:url');
   if(oexchangeUrl != undefined) {
     url = oexchangeUrl;
     if(url == 'undefined') {
       url = window.location.href;
     }
   } else {
     url = window.location.href;
   }
   oexchangeDesc = $this.attr('share:description');
   if(oexchangeDesc != undefined) {
     description = '&description=';
     description += oexchangeDesc;
   }
   oexchangeTitle = $this.attr('share:title');
   if(oexchangeTitle != undefined) {
     title = '&title=';
     title += oexchangeTitle;
   }
   oexchangeImageurl = $this.attr('share:imageurl');
   if(oexchangeImageurl != undefined) {
     imageurl = '&imageurl=';
     imageurl += oexchangeImageurl;
   }
   viewUrl = "http://" + oexchange_url[2] + "/share?url=" + url + title + description + imageurl;
   window.open(viewUrl,'Popup','scrollbars=yes, resizable=yes, width=800, height=600');
  });
});
